# ---- Base Node ---- #
FROM ubuntu:16.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev git-core libevent-dev libboost-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev pkg-config sudo && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ---- #
FROM base as build
RUN git clone https://github.com/monacoinproject/monacoin.git /opt/monacoin
RUN cd /opt/monacoin/ && \
    ./autogen.sh && \
    ./configure --without-miniupnpc --disable-tests --enable-debug --with-gui=no
RUN cd /opt/monacoin/ && \
    make -j2

# ---- Release ---- #
FROM ubuntu:16.04 as release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y libboost-all-dev libdb4.8 libdb4.8++ libevent-dev libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r monacoin && useradd -r -m -g monacoin monacoin
RUN mkdir /data
RUN chown monacoin:monacoin /data
COPY --from=build /opt/monacoin/src/monacoind /usr/local/bin/monacoind
COPY --from=build /opt/monacoin/src/monacoin-cli /usr/local/bin/monacoin-cli
USER monacoin
VOLUME /data
EXPOSE 9402 9401
CMD ["/usr/local/bin/monacoind", "-datadir=/data", "-conf=/data/monacoin.conf", "-server", "-txindex", "-printtoconsole"]