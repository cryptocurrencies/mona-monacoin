This Dockerfile generates the latest Monacoind client used in server applications. It is anticipated that you would run the following to install it on your server:

Replacing /localdata with a location on your server where you want to keep the persistant data:
```sh
docker run -d -P --name monacoin -v /localdata:/data \
registry.gitlab.com/cryptocurrencies/mona-monacoin:latest
```